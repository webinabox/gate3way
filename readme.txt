=== Gate3way ===
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Gate3way network plugin.

== Description ==

Gate3way is a plugin that allows all the websites in the gate3way network to link their wordpress signup form to the gate3way central database.

== Installation ==

Just upload and activate your plugin in the plugins page.

Two custom pages will be created: registration/signup and privacy page. The users that want to sign up to your blog will be automatically redirected to the new registration page.

== Changelog ==
= 1.1 =
* The plugin should now update and sync with bitbucket
= 1.0 =
* First version of the plugin.
