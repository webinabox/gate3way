<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
 function registration_form( $username, $password, $email, $website, $first_name, $last_name, $rag_soc, $regione ) {
    
    $regioni = array(
  array('1' => 'Abruzzo'),
  array('2' => 'Basilicata'),
  array('3' => 'Calabria'),
  array('4' => 'Campania'),
  array('5' => 'Emilia - Romagna'),
  array('6' => 'Friuli - Venezia Giulia'),
  array('7' => 'Lazio'),
  array('8' => 'Liguria'),
  array('9' => 'Lombardia'),
  array('10' => 'Marche'),
  array('11' => 'Molise'),
  array('12' => 'Piemonte'),
  array('13' => 'Puglia'),
  array('14' => 'Sardegna'),
  array('15' => 'Sicilia'),
  array('16' => 'Toscana'),
  array('17' => 'Trentino - Alto Adige'),
  array('18' => 'Umbria'),
  array('19' => 'Valle d\'Aosta'),
  array('20' => 'Veneto')
);
    echo '
    <style>
    div {
        margin-bottom:2px;
    }
     
    input{
        margin-bottom:4px;
    }
    </style>
    ';
 
    echo '
    <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
    <div>
    <label style="width:400px;display:block;" for="username">Username <strong>*</strong></label>
    <input style="padding:0;" type="text" name="username" value="' . ( isset( $_POST['username'] ) ? $username : null ) . '">
    </div>
     
    <div>
    <label style="width:400px;display:block;" for="password">Password <strong>*</strong></label>
    <input style="width:250px;border: 1px solid #d1d1d1;height:20px;" type="password" name="password" value="' . ( isset( $_POST['password'] ) ? $password : null ) . '">
    </div>
     
    <div>
    <label style="width:400px;display:block;" for="email">Email <strong>*</strong></label>
    <input style="padding:0;" type="text" name="email" value="' . ( isset( $_POST['email']) ? $email : null ) . '">
    </div>
    
    <div>
    <label style="width:400px;display:block;" for="website">Ragione sociale<br />
    <small>(se si tratta di un soggetto privato lasciare vuoto)</small></label>
    <input style="padding:0;" type="text" name="rag_soc" value="' . ( isset( $_POST['rag_soc']) ? $rag_soc : null ) . '">
    </div>
     
    <div>
    <label style="width:400px;display:block;" for="firstname">Nome <strong>*</strong></label>
    <input style="padding:0;" type="text" name="fname" value="' . ( isset( $_POST['fname']) ? $first_name : null ) . '">
    </div>
     
    <div>
    <label style="width:400px;display:block;" for="website">Cognome <strong>*</strong></label>
    <input style="padding:0;" type="text" name="lname" value="' . ( isset( $_POST['lname']) ? $last_name : null ) . '">
    </div>
    
    <div>
    <label style="width:400px;display:block;" for="website">Regione <strong>*</strong></label>
    <select name="regione"><option style="color:black;" value="seleziona">seleziona</option>';
    foreach ($regioni as $regionez ){
	//$nomeregione=$idregione[$idregione];
	foreach ($regionez as $idregione => $nomeregione){
	    if ($idregione==$regione){$cheded=' selected="selected"';} else {$cheded="";}
	echo '<option style="color:black;" value="'.$idregione.'"'.$cheded.'>'.stripslashes($nomeregione).'</option>';
	}
    }
    $privacyperma=get_option("ess_privacy_page");
    echo '
    </select>
    </div>
    <div>
   
    <label style="width:400px;display:block;" for="privacy"> <input style="padding:0;" type="checkbox" name="privacy" /> Ho letto e accetto l\'<a href="'.$privacyperma.'">informativa sulla privacy</a> <strong>*</strong></label>
    </div>
    <input style="padding:0;" type="submit" name="submit" value="Registrati"/>
    </form>
    ';
}

function registration_validation( $privacy, $username, $password, $email, $website, $first_name, $last_name, $rag_soc, $regione )  {
global $reg_errors;
$reg_errors = new WP_Error;
if (!$privacy) {
    $reg_errors->add('privacy', 'E\' obbligatorio dare il consenso all\'informativa sulla privacy');
}
if ( empty( $username ) || empty( $password ) || empty( $email ) || $regione=="seleziona" || empty( $first_name )|| empty( $last_name )) {
    $reg_errors->add('field', 'Si prega di compilare i campi obbligatori');
}
if ( 4 > strlen( $username ) ) {
    $reg_errors->add( 'username_length', 'L\'username è troppo corto. Si prega di usare almeno 4 caratteri.' );
}
if ( username_exists( $username ) )
    $reg_errors->add('user_name', 'L\'username è già in uso. Si prega di sceglierne un altro.');
if ( ! validate_username( $username ) ) {
    $reg_errors->add( 'username_invalid', 'L\'username inserito non è valido. Si prega di sceglierne un altro.' );
}
if ( 5 > strlen( $password ) ) {
        $reg_errors->add( 'password', 'La password deve essere lunga almeno 5 caratteri.' );
    }
if ( !is_email( $email ) ) {
    $reg_errors->add( 'email_invalid', 'Si prega di inserire un indirizzo e-mail valido.' );
}
if ( email_exists( $email ) ) {
    $reg_errors->add( 'email', 'L\'email risulta già in uso su questo sito.' );
}
if ($rag_soc=="" ) {
    $privato=1;
} else {
    $privato=0;
}

if ( is_wp_error( $reg_errors ) ) {
 
    foreach ( $reg_errors->get_error_messages() as $error ) {
     
        echo '<div>';
        echo '<strong>ERRORE</strong>:';
        echo $error . '<br/>';
        echo '</div>';
         
    }
 
}
}
function complete_registration() {
    global $alreadydone, $reg_errors, $username, $password, $email, $website, $first_name, $last_name, $rag_soc, $regione;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        $userdata = array(
        'user_login'    =>   $username,
        'user_email'    =>   $email,
        'user_pass'     =>   $password,
        'user_url'      =>   $website,
        'first_name'    =>   $first_name,
        'last_name'     =>   $last_name,
	'rag_soc'       =>   $rag_soc,
	'regione'       =>   $regione,
	'role'	=>   'author'
        );
        $user = wp_insert_user( $userdata );
	
	//rest
	$id_sito=get_option("ess_id_sito");
	$id_cat=0;
	$id_reg=$regione;
	$id_rest = md5($id_cat.$id_reg.$id_sito.$email);
	$auth=$_SERVER["HTTP_USER_AGENT"].":".$_SERVER["HTTP_HOST"].":".$_SERVER["REMOTE_ADDR"].":".date("Y-m-d-H-i");
	
		
		if (trim($rag_soc)=="" ) {
		    $privato=1;
		} else {
		    $privato=0;
		}
		$rest_password=base64_decode('MjQxS3pfeXp5');
		$request = new HTTP_Request2('http://www.emailselfservice.it/rest/rest/privati.php', HTTP_Request2::METHOD_POST);
		$request->setAuth('restuser', $rest_password);

		$request->addPostParameter('action', 'add');
		
		$request->addPostParameter('id', $id_rest);
		$request->addPostParameter('id_cat', $id_cat);
		$request->addPostParameter('id_reg', $regione);
		$request->addPostParameter('id_sito', $id_sito);
		$request->addPostParameter('email', $email);
		$request->addPostParameter('riferimento', $first_name." ".$last_name);
		$request->addPostParameter('blacklist', '');
		$request->addPostParameter('rag_soc', $rag_soc);
		$request->addPostParameter('privato', $privato);
                $request->addPostParameter('auth', $auth);
                $request->send();
		
        echo 'Registrazione completata!<br /><br />
	Riceverai presto una mail con i tuoi dati d\'accesso.';
	
	//mando la mail
	$registrazione=get_option("ess_redirect");
	add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
	
	$testomail="Gentile ".$first_name." ".$last_name.",<br />
	ti ringraziamo per esserti iscritto al nostro sito web.<br /><br />
	Ecco i suoi dati d'accesso:<br />
	Username: ".$username."<br />
	Password: ".$password."<br /><br />
	Potrà effettuare il login dal seguente url: ".$registrazione."<br /><br />
	A presto,<br />
	Lo staff";
	$blog_title =  get_bloginfo("wpurl");
	wp_mail($email, 'Registrazione a '.$blog_title, $testomail);
	$alreadydone="si";
    }
}
function custom_registration_function() {
    if ( isset($_POST['submit'] ) ) {
        registration_validation(
	$_POST['privacy'],
        $_POST['username'],
        $_POST['password'],
        $_POST['email'],
        $_POST['website'],
        $_POST['fname'],
        $_POST['lname'],
	$_POST['rag_soc'],
	$_POST['regione']
        );
         
        // sanitize user form input
        global $alreadydone, $username, $password, $email, $website, $first_name, $last_name, $regione, $rag_soc;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $website    =   esc_url( $_POST['website'] );
        $first_name =   sanitize_text_field( $_POST['fname'] );
        $last_name  =   sanitize_text_field( $_POST['lname'] );
	$regione=$_POST['regione'];
	$rag_soc=   sanitize_text_field( $_POST['rag_soc'] );
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
        $username,
        $password,
        $email,
        $website,
        $first_name,
        $last_name,
        $rag_soc,
        $regione
        );
    }
 if ($alreadydone!="si"){
    registration_form(
        $username,
        $passwordx,
        $email,
        $website,
        $first_name,
        $last_name,
        $rag_soc,
        $regione
        );
 }
}

function activation_hook() {
    $plugin = plugin_basename( __FILE__ );
    $rest_password=base64_decode('MjQxS3pfeXp5');
    //controllo se questo sito c'è già in ess
    $dominio=str_replace("http://","",get_site_url());      
    $ch = curl_init();
    $optArray = array(
	CURLOPT_URL => 'http://www.emailselfservice.it/rest/rest/siti.php?sito='.$dominio,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_USERPWD => "restuser:".$rest_password,
	CURLOPT_HTTPAUTH => CURLAUTH_BASIC
    );
    curl_setopt_array($ch, $optArray);
    $id_sito = curl_exec($ch);
    
    //creo la pagina di registrazione
    $post = array(
 
  'post_content'   => '[ess_custom_registration]',
  'post_name'      => 'registrazione',
  'post_title'     => 'Registrazione',
  'post_status'    => 'publish',
  'post_type'      => 'page',
  'comment_status' => 'closed',
  'ping_status' => 'closed'
);  
    $post_id=wp_insert_post($post);
    
    //inserisco la pagina privacy
    $blog_title = get_bloginfo();
    $testoprivacy=<<<EOD
<p>In questa pagina potete trovare tutte le informazioni relative alle modalità di gestione del sito <strong>$blog_title</strong> riferimento al trattamento dei dati personali degli utenti che lo consultano.<br />
La presente informativa viene resa, anche ai sensi dell’art. 13 della D.lgs. n. 196/2003 (e successive modifiche) soltanto per il sito $blog_title e non anche per altri siti web eventualmente consultati dall’utente tramite link disponibili sul sito $blog_title</p>
<p><strong>Titolare del Trattamento</strong><br />
La consultazione del sito $blog_title può comportare il trattamento di dati relativi ad utenti identificati o identificabili.<br />
Il “titolare” del loro trattamento è Communication36 srl, con sede legale in via Reinach 8, 20159 Milano, P.Iva 03792370169</p>
<p><strong>Tipi di dati trattati e finalità del relativo trattamento</strong></p>
<p><em><strong>Dati di navigazione</strong> </em><br />
Durante la navigazione sul sito $blog_title i sistemi informatici e le procedure software che presiedono al funzionamento del sito, raccolgono alcuni dati personali la cui trasmissione è implicita nell’uso dei protocolli di comunicazione Internet.  Esempi di questa tipologia di dati sono gli indirizzi IP o i nomi a dominio dei computer utilizzati dagli utenti che si collegano al sito, gli indirizzi URI (Uniform Resource Identifier) delle risorse richieste, l’orario della richiesta, il metodo utilizzato nel sottoporre la richiesta al server, la dimensione del file ottenuto in risposta, il codice numerico che indica la risposta data dal server (buon fine, errore, ecc.) ed altre informazioni relative al sistema operativo e all’ambiente informatico dell’utente.  I dati di cui sopra, pur non essendo di per sé soli riconducibili ad utenti identificati, potrebbero permettere di identificare gli utenti a cui si riferiscono attraverso rielaborazioni con dati in possesso di terzi.  Essi vengono tuttavia utilizzati da Communication360 srl al solo fine di elaborare statistiche anonime sulla consultazione del sito e di controllare il corretto funzionamento di quest’ultimo e vengono cancellati immediatamente dopo l’elaborazione.  Tali dati potrebbero inoltre essere utilizzati per l’accertamento di responsabilità in caso di ipotetici reati informatici ai danni del sito.  A parte questa ipotesi, di regola, essi sono conservati per non più di sette giorni.</p>
<p><em><strong>Dati forniti volontariamente dall’utente</strong></em><br />
La raccolta di dati personali degli utenti può avvenire in maniera volontaria, attraverso l&#8217;invio di posta elettronica agli indirizzi riportati sul sito o attraverso la spedizione dei moduli elettronici presenti sullo stesso da parte degli utenti. L&#8217;invio di posta elettronica comporta la raccolta, da parte di Communication360 srl, dell&#8217;indirizzo email del mittente, necessario per rispondere alla richiesta, nonché egli eventuali altri dati personali contenuti nella comunicazione.  I dati personali raccolti attraverso questa modalità verranno utilizzati da Communication360 srl al solo fine di rispondere al messaggio di posta elettronica ricevuto e per eventuali successive comunicazioni correlate.  La spedizione di moduli elettronici per richieste di informazioni comporta la raccolta, da parte di Communication360 srl, dei dati personali inseriti nel modulo stesso.  I dati personali raccolti attraverso questa modalità saranno utilizzati da Communication360 srl al solo fine di dare riscontro alla richiesta di informazioni ricevuta e per eventuali successive comunicazioni correlate.</p>
<p><strong>Facoltatività del conferimento dei dati</strong><br />
A parte quanto specificato per i dati di navigazione, l’utente è libero di fornire i dati personali riportati nei moduli di richiesta.<br />
Il loro mancato conferimento può comportare l’impossibilità di ottenere quanto richiesto.</p>
<p><strong>Modalità del trattamento</strong><br />
I dati personali sono trattati con strumenti manuali ed automatizzati per il tempo strettamente necessario a conseguire gli scopi per cui sono stati raccolti. Specifiche misure di sicurezza sono adottate per prevenire la perdita dei dati, l’usi illecito o non corretto e l’accesso non autorizzato agli stessi.</p>
<p><strong>Diritti degli interessati</strong><br />
Ai sensi dell’art. 7 del D.lgs 196/2003 (e successive modifiche), gli utenti cui si riferiscono i dati personali hanno il diritto in qualunque momento di ottenere la conferma dell&#8217;esistenza o meno dei medesimi dati e di conoscerne il contenuto e l&#8217;origine, anche per verificarne l&#8217;esattezza o chiederne l&#8217;integrazione o l&#8217;aggiornamento, oppure la rettificazione.  Ai sensi del medesimo articolo gli utenti hanno inoltre il diritto di chiedere la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge nonché di opporsi in ogni caso, per motivi legittimi, al loro trattamento.  Le richieste vanno inviate a Communication360 srl, Communication36 srl, con sede legale in via Reinach 8, 20159 Milano</p>
EOD;

    //creo la pagina di registrazione
    $post2 = array(
 
  'post_content'   => $testoprivacy,
  'post_name'      => 'privacy',
  'post_title'     => 'Privacy',
  'post_status'    => 'publish',
  'post_type'      => 'page',
  'comment_status' => 'closed',
  'ping_status' => 'closed'
);  
    $post_id2=wp_insert_post($post2);
    
    if (!$post_id || !$post_id2){
        //se non inserisce la pagina disattivo tutto
        if( is_plugin_active($plugin) ) {
            deactivate_plugins( $plugin );
            wp_die( "<strong>Si è verificato un problema!</strong> Il plugin non può essere attivato." );
        } else {
            wp_die( "<strong>Si è verificaget_optionto un problema!</strong> Il plugin non può essere attivato." );
        }
    } else {
        add_option("ess_page_id",$post_id);
	add_option("ess_privacy_id",$post_id2);
	add_option("ess_privacy_page",get_permalink($post_id2));
	add_option("ess_id_sito",$id_sito);
        add_option("ess_redirect",get_permalink($post_id));
    }
    
}

function deactivation_hook() {
    //cancello la pagina di registrazione e le opzioni
        $post_id=get_option("ess_page_id");
        wp_delete_post($post_id,true);
        delete_option("ess_page_id");
	$post_id2=get_option("ess_privacy_id");
        wp_delete_post($post_id2,true);
        delete_option("ess_privacy_id");
        delete_option("ess_redirect");
	delete_option("ess_id_sito");
    
}
?>
