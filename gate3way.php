<?php
/*
  Plugin Name: Gate3way
  Plugin URI: https://webinabox@bitbucket.org/webinabox/gate3way.git
  Git URI: https://webinabox@bitbucket.org/webinabox/gate3way.git
  Description: Registrazione in remoto a Gate3way
  Version: 1.1
  Author: Serena Villa
  Author URI: http://www.josie.it
 */

require_once 'HTTP/Request2.php';
include 'wp_mail_smtp.php';
include 'functions.php';

//autoupdate
add_action( 'plugins_loaded', 'myplugin_git_updater' );
function myplugin_git_updater() {
    if ( is_admin() && !class_exists( 'GPU_Controller' ) ) {
        require_once dirname( __FILE__ ) . '/git-plugin-updates/git-plugin-updates.php';
        add_action( 'plugins_loaded', 'GPU_Controller::get_instance', 20 );
    }
}

register_activation_hook( __FILE__, 'activation_hook' );
register_deactivation_hook( __FILE__, 'deactivation_hook' );

// Registro lo shortcode
add_shortcode( 'ess_custom_registration', 'custom_registration_shortcode' );
 
// Cosa fa lo shortcode?
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}
//Redirigo la pagina registrazione
add_action( 'login_form_register', 'ess_catch_register' );

function ess_catch_register()
{
    $permalink= get_option("ess_redirect");
    wp_redirect( $permalink  );
    exit();
}
