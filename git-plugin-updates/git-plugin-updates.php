<?php
/**
 * ## TESTING
 * Change version number above to 1.0 to test updates.
 */

/**
 * Verify we're in wp-admin -- plugin doesn't need to load in front-end.
 * Verify that we're running WordPress 3.2 (which enforces PHP 5.2.4).
 */
if ( is_admin() && version_compare( $GLOBALS['wp_version'], '3.2', '>=' ) ) :

	// Load plugin classes and instantiate the plugin.
	require_once dirname( __FILE__ ) . '/includes/class-controller.php';
	require_once dirname( __FILE__ ) . '/includes/class-updater.php';
	require_once dirname( __FILE__ ) . '/includes/class-updater-github.php';
	require_once dirname( __FILE__ ) . '/includes/class-updater-bitbucket.php';

	add_action( 'plugins_loaded', 'GPU_Controller::get_instance', 5 );

endif;
